import{test, expect} from '@playwright/test';
import{ LoginPage } from '../page-object/LoginPage';
import { HomePage } from '../page-object/HomePage';
import { CartPage } from '../page-object/CartPage';
import { CheckoutPage } from '../page-object/CheckouPage';



test('addToCart', async ({page}) => {
    const loginPage =new LoginPage(page);
    const homePage =new HomePage(page);
    const cartPage =new CartPage(page);
    const checkoutPage =new CheckoutPage(page);
    
    //Login

    //Load the URL
    await loginPage.loadTheUrl();
    //Enter the UserName
    await loginPage.enterTheUserName();
    //Enter the Password
    await loginPage.enterThePassword();
    //CLick on the Login button
    await loginPage.clickOnTheLoginBUtton();
    //Verify the Homepage is loaded
    await homePage.verifyTheHomePageIsLoaded();

    //Add products to the Cart

    //Select the First product on the Homepage
    await homePage.selectTheFirstProductAndClickOnTheAddToCartButton();

    //Select the Last product on the Homepage
    await homePage.selectTheLastProductAndClickOnTheAddToCartButton();

    //Click on the Cart button
    await homePage.clickOnTheCartButton();

    //Verify the Cart page is opened
    await cartPage.verifyTheCartPageIsLoaded();

    //Cart page

    //Remove the first product
    await cartPage.clickOnTheRemoveButton();

    //Click on the Checkout button
    await cartPage.clickOnTheCheckoutButton();

    //Checkout Page

    //Verify the Checkout: Your Information page is loaded
    await checkoutPage.verifyTheCheckOutInformationIsLoaded();
    //enterTheFirstName
    await checkoutPage.enterTheFirstName();

    //enterTheLastName
    await checkoutPage.enterTheLastName();

    //enterThePostalCode
    await checkoutPage.enterThePostalCode();

    //clickOnTheContinueButton
    await checkoutPage.clickOnTheContinueButton();

    //Verify the Checkout: Overview
    await checkoutPage.verifyTheCheckoutOverViewPageIsLoaded();

    //Verify the Total price
    await checkoutPage.verifyTheTotalPrice();

    //clickOnTheFinishButton
    await checkoutPage.clickOnTheFinishButton();

    //Verify "Thanks for Your order" message
    await checkoutPage.verifyTheOrderPlacedMessage();

    //Logout
    
    //Click on the menu button
    await checkoutPage.clickOnTheMeneuButton();

    //Click on the logout button
    await checkoutPage.clickOnTheLogoutButton();
    
})