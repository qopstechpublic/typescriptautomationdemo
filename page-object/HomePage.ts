import { Page,Locator,expect } from "@playwright/test";

export class HomePage{
    readonly page:Page
    readonly addToCartButtonOne: Locator
    readonly addToCartButtonTwo: Locator
    readonly cartButton: Locator
    readonly verifyHomePage: Locator


    constructor(page:Page){
        this.page =page;
        this.addToCartButtonOne= page.locator("(//button[text()='Add to cart'])[1]");
        this.addToCartButtonTwo= page.locator("(//button[text()='Add to cart'])[5]");
        
        this.cartButton= page.locator('#shopping_cart_container');
        this.verifyHomePage= page.getByText('Products');
    }

    async verifyTheHomePageIsLoaded(){
        await expect(this.verifyHomePage).toBeVisible();
    }


    async selectTheFirstProductAndClickOnTheAddToCartButton(){
        await this.addToCartButtonOne.click();
    }

    async selectTheLastProductAndClickOnTheAddToCartButton(){
        const elemet= this.page.locator("//a[text()='LinkedIn']");
        
        elemet.scrollIntoViewIfNeeded();
        
        await this.addToCartButtonTwo.click({timeout:3000});
    }

    async clickOnTheCartButton(){
        await this.cartButton.click();
    }
}