import { Page,Locator,expect } from "@playwright/test";

export class CartPage{
    readonly page: Page
    readonly firstRemoveButton: Locator
    readonly checkoutButton: Locator
    readonly verifyTheCartPage: Locator


    constructor(page:Page){
        this.page=page;
        this.firstRemoveButton=page.locator('#remove-sauce-labs-backpack');
        this.checkoutButton= page.locator('#checkout');
        this.verifyTheCartPage= page.getByText('Your Cart');
    }


    async clickOnTheRemoveButton(){
        await this.firstRemoveButton.click();
    }

    async clickOnTheCheckoutButton(){
        await this.checkoutButton.click();
    }
    async verifyTheCartPageIsLoaded(){
        await expect(this.verifyTheCartPage).toBeVisible();
    }


}