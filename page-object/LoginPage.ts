import { Page,expect,Locator } from "@playwright/test";




export class LoginPage{
    readonly page: Page
    readonly userName: Locator
    readonly password: Locator
    readonly loginButton: Locator

    constructor(page:Page){
        this.page=page;
        this.userName= page.locator('#user-name');
        this.password= page.locator('#password');
        this.loginButton= page.locator('#login-button');
    }



    async loadTheUrl(){
        await this.page.goto('https://www.saucedemo.com/');
    }

    async enterTheUserName(){
        await this.userName.fill('standard_user');
    }

    async enterThePassword(){
        await this.password.fill('secret_sauce');
    }

    async clickOnTheLoginBUtton(){
        await this.loginButton.click();
    }
}