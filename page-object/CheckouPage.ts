import { Page,expect,Locator } from "@playwright/test";
import { timeStamp } from "console";

export class CheckoutPage{
    readonly page:Page
    readonly firstName: Locator
    readonly lastName: Locator
    readonly postalCode: Locator
    readonly continueButton: Locator
    readonly finishButton: Locator
    readonly checkoutYourInfo: Locator
    readonly checkoutOverView: Locator
    readonly totalPrice: Locator
    readonly thanksForOrder: Locator
    readonly menuButton: Locator
    readonly logoutBUtton: Locator

    constructor(page: Page){
        this.page=page;
        this.firstName= page.locator('#first-name');
        this.lastName= page.locator('#last-name');
        this.postalCode= page.locator('#postal-code');
        this.continueButton= page.locator('#continue');
        this.finishButton= page.locator('#finish');
        this.checkoutYourInfo= page.getByText('Checkout: Your Information');
        this.checkoutOverView= page.getByText('Checkout: Overview');
        this.totalPrice= page.locator("//div[text()='17.27']");
        this.thanksForOrder= page.getByText('Thank you for your order!');
        this.menuButton= page.locator('#react-burger-menu-btn');
        this.logoutBUtton= page.locator('#logout_sidebar_link');
    }

    async verifyTheCheckOutInformationIsLoaded(){
        await expect(this.checkoutYourInfo).toBeVisible();
    }
    async enterTheFirstName(){
        await this.firstName.fill('Gokul');
    }
    async enterTheLastName(){
        await this.lastName.fill('kannan');
    }

    async enterThePostalCode(){
        await this.postalCode.fill('111111');
    }

    async clickOnTheContinueButton(){
        await this.continueButton.click();
    }
    async verifyTheCheckoutOverViewPageIsLoaded(){
        await expect(this.checkoutOverView).toBeVisible();
    }
    async verifyTheTotalPrice(){
        await expect(this.totalPrice).toBeVisible();
    }

    async clickOnTheFinishButton(){
        await this.finishButton.click();
    }
    async verifyTheOrderPlacedMessage(){
        await expect(this.thanksForOrder).toBeVisible();
    }

    async clickOnTheMeneuButton(){
        await this.menuButton.click();
    }

    async clickOnTheLogoutButton(){
        await this.logoutBUtton.click();
    }

}